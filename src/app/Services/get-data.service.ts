import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {
  private url: string = "";  
  constructor(private inyect_http: HttpClient) { }
  getData(){
    this.url = 'https://dev.tuten.cl/TutenREST/rest/user/contacto%40tuten.cl/current=true';        
    const headers = new HttpHeaders()
   //.set('Content-Type', 'application/json')      
   .set("app","APP_BCK")
   .set("Accept","application/json")
   .set("adminemail","testapis@tuten.cl")   
   .set("token",localStorage.getItem("token"))
   .set("Access-Control-Allow-Method","GET")
   
   ;
    let params = "";       
     return this.inyect_http.get(this.url,{headers:headers});
  }
}
