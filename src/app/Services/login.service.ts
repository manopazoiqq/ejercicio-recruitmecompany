import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {  
  private url: string = "";
  constructor(private inyect_http: HttpClient) { }
  acceder(email: string,password: string){
    this.url = 'https://dev.tuten.cl/TutenREST/rest/user/'+email;    
    const headers = new HttpHeaders()
   .set('Content-Type', 'application/json')   
   .set("password",password)
   .set("app","APP_BCK")
   .set("Accept","application/json");
    let params = "";       
     return this.inyect_http.put(this.url,params,{headers:headers});
  }
}
