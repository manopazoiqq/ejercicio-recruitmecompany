import { Component, OnInit } from '@angular/core';
import { GetDataService } from 'src/app/Services/get-data.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  lista:any[];
  constructor(private service: GetDataService) { }

  ngOnInit() {
    this.getData();
  }
  getData(){
    this.service.getData().subscribe(resp=>{
      this.lista = resp as any;
    })
  }

}
