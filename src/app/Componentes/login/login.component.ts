import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/Services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string = "";  
  password: string = "";
  constructor(private service : LoginService, private router: Router) { }

  ngOnInit() {
  }
  acceder(){
    this.service.acceder(this.email,this.password).subscribe(
    resp => {            
      localStorage.setItem("token","");       
      this.router.navigate(['principal']);   
    }, 
    error =>{
      alert("Error");
    })       
  }
}
